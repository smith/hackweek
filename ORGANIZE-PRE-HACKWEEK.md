# Preparation:


0. Check that there is no release happening that week.

1. Review process

2. Present hackweek at all hands 2 months earlier.

    - Present process & pad where we are going to add proposals.

3. Public place for folks to add the projects they want to do during the hackweek. The fields would be:

 - Project/team name
 - Main point of contact for the project/team (timezone you are in)
 - Summary of the work you want to do
 - Skills you need (for example, someone who knows onionperf, designer, translator,
code in rust - things like this)
  - Comments
  - (for the "call for help" phase) A list with each person that wants to contribute:

    - name and the skill they can contribute


4. Once we have the above ^^^ we should send an email out to everyone at Tor (tor-internal) telling people to start adding their ideas to the page. Give people 2 to 3 weeks to add projects.

5. Call for help: We keep this going for a few weeks, when we decide that we have enough projects listed there we can start the 'call for help' so teams can get
the help they need. Send mail to tor-project@  and otf-talk@ and publish in teams pads but do not post in social media.
