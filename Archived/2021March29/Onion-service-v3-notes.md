# Tor Hackweek Project: Onion service v3 support for arti

Summary: arti currently does not do v3 onion services. Let's do some solid work towards making that possible.

Skills: Tor protocol, Onion services protocol, Rust, arti

# Team:

* ASN (UTC-3)

# Breakdown and suggested hacking order:

* tor-llcrypto crate: identify low-level operations needed for  v3 onion services. (key blinding)
* tor-cell crate: suppport new relay message types.  They should get their own module under relaycell::msg.   We should have encode, decode, fuzz, and test  vector support for them.
     - Should we look for ways to make the cell parsing and encoding more generic so that other extensions can define their own cells?  I think that would be a good ide eventually, but probably not for now.
* tor-netdoc crate: support v3 service descriptors.  This should include outer and inner forms. [asn first target]
* tor-netdir crate: we need support for the hash index ring.
* tor-circmgr crate:  **
     - need support for building a circuit with a given exit
     - need support for saying "nobody else can use this circuit!"
     - need support for saying "this circuit is a valid open connection to (x.onion)", and for saying "i am building a connection to (x.onion), please wait.
* tor-dirmgr crate:
     - Support for parallel v3 onion descriptor lookup.  This involves adding a new ClientRequest type, and probably refactoring the ClientRequest trait so it can handle the things that make onion descriptor lookup different.
* tor-proto crate: we need support for taking a circuit and then doing an arbitrary send-this-cell-then-wait-for-that-cell handshake.   **
* tor-proto crate: need support for extending a circuit with an end-to-end encryption hop, as used with onion services.    **
* tor-proto crate: possibly, move some or all of crypto module into a different crate?  Some might belong in tor-llcryppto, but not all.
* new tor-hsclient crate: given a cirmgr, a dirmgr, and an onion service name, it should be able to it get or construct a completed rendezvous circuit to that onion service.  Its interface can be similar to circmgr.  Use APIs in tor-retry for anything we want to attempt more than once.
     - This is probably where any descriptor cache should live.
* tor-client crate: When told to connect to an onion address, get the circuit from tor-hsclient. 

# Design notes:

* Don't do anything with v2 onions at all.
* Client-side support for onion services should be under an hs-client feature; server-side support should be under an hs-server feature.  Common code should be under an "hs" feature and enabled by either.
* New functionality in tor-proto should probably NOT be hs-specific.  That is, most of the hs-specific protocol handling stuff should be done in a different crate.
* Parsing shouldn't do crypto: decryption and signature-checking should happen in a post-parsing step. 
     - This means that for many message types, the parse operation will just put the body in a buffer, and the real parsing will happen after the decryption
     - Use different types to prevent confusion here: See current use of the SelfSigned triat in tor-netdir to ensure that signatures are explicitly checked or explicitly ignored.


   



  


