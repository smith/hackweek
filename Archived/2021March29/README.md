## First online Hackweek: March 29th to April 2nd - 2021

 * **When**: Monday March 29th to Friday April 2nd - starts each day at 1500UTC.
 * **Where**: irc & BBB links will be available for the projects

## 1 Prometheus alerts for anti-censorship metrics

- Notes: [anticensorship alerting](anti-censorship-alerting.md)
- Contact: cohosh and anarcat
- Summary: We have BridgeDB exporting prometheus metrics so far, and we could implement this for Snowflake. It would be great if we could get alerts when usage changes to notify us of possible censorship events. Somewhat related, it would also be nice to get alerts when default bridge usage drops off suddenly or directly connecting Tor users from different regions.
- Skills: maybe Go (for changes to snowflake), maybe Python for other services, some sysadmin experience to figure out how to do the alerts, metrics pipeline experience.

## 2 Prototype network-namespace-based torsocks

- Notes: [torsocks](torsocks.md)
- Contact: jnewsome and/or dgoulet
- Summary: Use network namespaces (or maybe something else?) to run target software in an environment where it can't talk to the real network; it can only talk to the tor socks port and/or some "shim" adapter. Might be able to remove or lessen dependence on LD_PRELOAD (which isn't available everywhere, can be "escaped", and can be a bit fragile). If we continued to use LD_PRELOAD could at least be used to prevent accidental connections to the real network.
- Skills: C, familiarity with network namespaces and/or LD_PRELOAD would be helpful. New code could potentially be written in Rust.

## 3 Add support for UDP sockets over onion services, possibly with enabling support for WebRTC in Tor Browser

- Notes: [Add support for UDP sockets over onion services](Add-support-for-upd-notes.md)
- Contact: sysrqb (UTC-5(-ish))
- Skills: C/C++, familiar with tor and/or Firefox and/or webrtc
- Goal: Present Hack Week results via Tor Browser on BBB
- Intermediate Tor steps:
  - Add support for relaying UDP datagrams
  - Add support for configuring UDP onion services
- Intermediate Tor Browser steps:
  - Enable WebRTC
  - ?

## 4 Visualize Tor metrics's data in ways that it can be useful for community

- Notes: [Visualize Tor metrics's data](Visualize-tor-metrics-data-notes.md)
- Contact: gus and gaba (UTC-3)
- Skills: data visualization
- Summary: The goal is to have a dashboard with Tor usage per country in a way that is easy to see big changes happening. Right now we need to select each country to see the Tor usage. It would be good to have a way to see all the countries and the onews where usage is increasing (via bridge and direct connection)

## 5 Prototype Rust+Arti-based HTTP frontend cache for directory authorities

- Notes:  [Prototype Rust+Arti-based HTTP](Prototype-rust+arti-based-note.md)
- Contact: nickm (UTC-5)
- Skills: Rust hacking experience helpful.
- Summary: Directory authorities are under a lot of unnecessary load from excessive download requests.  We have other projects in mind to reduce those requests, but one workaround is to add a frontend cache in front of one or more of the authorities' HTTP ports.  With this project we'll write a Rust server use Arti's download and validation code to fetch directory information from an authority, and expose that information via a set of HTTP requests.  With luck, our code will be reuseable in the future when relays or authorities are rewritten in Rust.

## 6 Onionshare download accelerator

- Notes:  [Onionshare download accelerator](Onionshare-download-accelerator-notes.md)
- Contact: Mike (UTC-6) or maybe Micah (UTC-8?)
- Skills: Python, HTTP/1.1, SOCKS
- Journalists are requesting ways to download large files from onion services faster. This is actually possible already with HTTP, without any Tor client modifications. By splitting up requests into 250k chunks using HTTP range requests, and using SOCKS username and password to allocate these requests properly onto different circuits, we can hit ~6 megabytes/sec per guard (so ~12 megabytes/sec with 2 gaurds), with a custom HTTP download acelerator. This download accelerator could be provided as part of onionshare. For more details, see https://github.com/micahflee/onionshare/issues/1295.

## 7 Vanguards doc updates, bugfixes, packages for onionshare and/or Tor Browser

- Notes:  [Vanguards doc updates, bugfixes, packages](Vanguards-doc-updates-notes.md)
- Contacts: Mike (UTC-6)
- Skills: Python, tech doc writing and/or review, packaging/build eng
- Summary: The vanguards Tor control port addon provides defense against Guard discovery attacks, as well as other attacks against onion services, onion clients, and even Tor Browser exit traffic. Vanguards is in need of some doc updates, bugfixes, and we could even package it for one of onionshare or Tor Browser. Packaging it with Onionshare also helps improve the security properties of the above download accelerator item. See the bugtracker for specific items: https://github.com/mikeperry-tor/vanguards/issues

## 8 Circuit Padding Simulator

- Notes: [Circuit Padding Simulator](Circuit-padding-simulator-notes.md)
- Contacts: Mike (UTC-6). Or maybe Tobias Pulls (UTC+1), or maybe asn (UTC+1)
- Skills: Tor unit test framework, Tor C, maybe python
- Summary: The Tor circuit padding framework is under active use by researchers to improve padding defenses against website traffic fingerprinting, and onion service circuit fingerprinting. The simulator is in need of update to the latest Tor release, as well as in need of performance improvements, and a more accurate way to deal with time-based features. See https://github.com/pylls/circpad-sim/blob/master/README.md

## 9 Onion service v3 support for arti

- Notes:  [Onion service v3 support for arti](Onion-service-v3-notes.md)
- Contacts: asn (UTC-3)
- Skills: Onion services protocol, Rust, arti
- Summary: arti currently does not do v3 onion services. Let's do some solid work towards making that possible.

## 10 Network Health tooling using Rust and arti

- Notes: [Network Health tooling using Rust and arti](Network-health-tooling-notes.md)
- Contacts: dgoulet (UTC-5)
- Skills: Love, Passion and Legos
- Summary: Network health has a series of tools in order to query the consensus, search for patterns and correlate relays. This project is to rewrite some of them into one unified tool written in Rust and using arti for any Tor related interactions such as looking up the consensus or creating circuits for testing connectivity.
