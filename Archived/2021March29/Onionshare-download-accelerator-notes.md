# Tor Hackweek Project: Onionshare download accelerator

Summary: Journalists are requesting ways to download large files from onion services faster. This is actually possible already with HTTP, without any Tor client modifications. By splitting up requests into 250k chunks using HTTP range requests, and using SOCKS username and password to allocate these requests properly onto different circuits, we can hit ~6 megabytes/sec per guard (so ~12 megabytes/sec with 2 gaurds), with a custom HTTP download acelerator. This download accelerator could be provided as part of onionshare. For more details, see [issues-1295][https://github.com/micahflee/onionshare/issues/1295].

Skills Needed: Python, HTTP/1.1, SOCKS

# Team:

* Mike (UTC-5)
* Saptak (UTC +5:30)

### Message mikeperry on OFTC IRC or email if there is interest in this.

