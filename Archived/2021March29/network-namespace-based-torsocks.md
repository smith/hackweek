# Tor Hackweek Project: Prototype network-namespace-based torsocks

Summary: Use network namespaces (or maybe something else?) to run target software in an environment where it can't talk to the real network; it can only talk to the tor socks port and/or some "shim" adapter. Might be able to remove or lessen dependence on LD_PRELOAD (which isn't available everywhere, can be "escaped", and can be a bit fragile). If we continued to use LD_PRELOAD could at least be used to prevent accidental connections to the real network.

Skills: C, familiarity with network namespaces and/or LD_PRELOAD would be helpful. New code could potentially be written in Rust.

# Team:

- jnewsome
- dgoulet
- boklm [boklm/torsocks-netns](https://gitlab.torproject.org/boklm/torsocks-netns)


### Where to meet: irc initially (jnewsome)

 # Problems to address:

- Can't be completely confident that LD_PRELOAD intercepts all network requests. 
- LD_PRELOAD can cause surprising breakages
- (Maybe) DNS leaks
    
   # Solutions to explore:

    - Network namespaces: Put target process(es) in a network namespace that can only talk to torsocks, getting   ?higher confidence that there are no leaks. Stretch: create a synthetic network adapter that talks to tor, so we don't need to use LD_PRELOAD at all.
    - SECCOMP: Create a seccomp filter that prevents network-related syscalls that don't originate from the LD_PRELOADd shim, getting higher confidence that there are no leaks. Stretch: use ptrace to rewrite network-related syscalls instead of using LD_PRELOAD.

   # Alternative/fallback: 

    - Go through torsocks issues and increment/fix what we can through incremental improvements and cut a release. [link to issue](https://gitlab.torproject.org/tpo/core/torsocks/-/issues)

   # Relevant issues:

    - FR to disable network: [issues-26889](https://gitlab.torproject.org/tpo/core/torsocks/-/issues/26889). This mentions that `firejail` could be  used for this. Looking at the man page for firejail, `--protocol=unix` seems like it'd do what we want.
    - irssi: [issues-11727](https://gitlab.torproject.org/tpo/core/torsocks/-/issues/11727). I'd thought there was a DNS leak, but maybe I misremembered. It looks like the issue is each process getting its own onion-address-resolution-table + irssi using multiple processes.
    - torsocks support for unix sockets: [issues-14132](https://gitlab.torproject.org/tpo/core/torsocks/-/issues/14132). (This would be let us disable net access completely)

  # Notes:

    - tor notes for how to set up a transparent proxy: [TransparentProxy](https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TransparentProxy)
    - Maybe we could write a wrapper script to set up a network namespace, configure all traffic in that ns to go through the transparent proxy, and then run the target program(s) in the namespace?

  # Results:

    - torsocks-netns: [torsocks-netns](https://gitlab.torproject.org/boklm/torsocks-netns:)
        - Wrapper for torsocks that protects against inadvertent leaks.
        - Puts torsocks + application in an empty network namespace and "smuggles" out to the tor socks port over a unix-pipe tunnel.
        - No root or priveleges required!
    - Proofs of concept without torsocks/LD_PRELOAD:
        - Network namespace with ip tables rules + proxy (implemented via redsocks) to funnel everything to either tor's socks port or tor's DNS port. (Root/privelege required?)
        - Tried using tor's transparent proxy functionality + iptables rules in a network namespace, but couldn't get it working. Creating a bridged adapter requires ~root. (Could use a priveleged binary like firejail if installed. firejail's default config doesn't allow it, though)
    - Proof of concept using tor's shared onion pool (to address [issues-11727](https://gitlab.torproject.org/tpo/core/torsocks/-/issues/11727), fixing torsocks+irssi)
        - torsocks handles resolving onion addresses using "onion cookies"
        - irssi uses different processes to resolve vs connect, so doesn't work
        - tor now *natively* supports onion cookies
        - Modified torsocks to use tor's native onion cookie support and successfully connected to an onion irc server

  # Questions/follow-ups from demo:

    - Jeremy: how does it compare to [orjail](https://github.com/orjail/orjail)
    - Jeremy: stream isolation in these modes? (-i with torsocks)
    - Matt: (torsocks.conf supports 'AllowOutboundLocalhost 0|1|2')
    - Nick: Tor always returns its local addresses from a given range specified in VirtualAddrNetworkIPv[46] , so you could in theory detect them like that. 
defaults are 127.192.0.0/10 and [FE80::]/10
    

