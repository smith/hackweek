# Tor Hackweek Project: Network Health tooling using Rust and arti

Summary: Network health has a series of tools in order to query the consensus,
search for patterns and correlate relays. This project is to rewrite some of
them into one unified tool written in Rust and using arti for any Tor related
interactions such as looking up the consensus or creating circuits for testing
connectivity.

Skills: Love, Passion and Legos

# Team:

* dgoulet
* juga?

# Feature ideas:

* Search relays with in the latest consensus by:
    - Fingerprint (RSA and/or ED)
    - Contact info (pattern, regex?)
    - Bandwidth values
    - AS
    - Nickname (pattern, regex)
    - Versions
    - Flags
    - ...
* Network stats
    - Relay counting by flags or versions
    - Extra info information such as "how many overload relays do we have"
    - Count onion service bandwidth
* Bad relays config
    - Generate bad relay dirauth configuration
* Network test
    - Test Exit DNS
    - Test ORPort/Dirport
    - Test connectivity IPv4/IPv6

# Nick's Notes:

* For "network test", probably best to use tor-proto crate for low-level fine-grained network usage.
    - Possibly, give tor-circmgr the ability to create circuits with specific paths?
    - Possibly, consider `tracing` crate for fine-grained events during testing, to analyze timing?
    - Look for opportunities to make these APIs simpler or better.
* For search and stats:
    - For some of this info, will need to teach tor-dirmgr about fetching ns consensus and routerdesc documents.  Also will need to teach tor-netdoc about fetching and parsing ns consensus documents.
         - This may warrant refactoring tor-dirmgr.
    - Consider sqlite or similar to build a relational database of decoded relay information.
    - Consider tools to export the existing state of the network as json, xml, or something else so that arbitrary other scripting tools can handle it.
    - Consider clap as a CLI-building tool.
    - It might be convenient to teach tor-dirmgr to operate in a "cache-only" mode that just looks at the cache of another dirmgr, and doesn't fetch the directory.  Ideally, this would be able to run concurrently with another process that is actually updating the directory.
    - For future-proofing, consider making a variant routerdesc/consensus parser that retains recognized data and allows searching it.     
