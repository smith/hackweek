# Tor Hackweek Project: Vanguards doc updates, bugfixes, packages for onionshare and/or Tor Browser

Summary: The vanguards Tor control port addon provides defense against Guard discovery attacks, as well as other attacks against onion services, onion clients, and even Tor Browser exit traffic. Vanguards is in need of some doc updates, bugfixes, and we could even package it for one of onionshare or Tor Browser. Packaging it with Onionshare also helps improve the security properties of the above download accelerator item. See the bugtracker for specific items: [issues here](https://github.com/mikeperry-tor/vanguards/issues)

Skills Needed: Python, tech doc writing and/or review, packaging/build eng

# Team:

* Mike (UTC-5)
