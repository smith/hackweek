# Tor Hackweek Project: Add support for UDP sockets over onion services, possibly with enabling support for WebRTC in Tor Browser

Summary: 

   -  Goal: Present Hack Week results via Tor Browser on BBB

* Intermediate Tor steps: 

    - Add support for relaying UDP datagrams

    - Add support for configuring UDP onion services

* Intermediate Tor Browser steps: 

    - Enable WebRTC

    - ?

Skills Needed:  C/C++, familiar with tor and/or Firefox and/or webrtc

 # Team:

* sysrqb (UTC-5)

 **Notes:** Possibly, ask mike and nick for the notes we took on how to make Tor support UDP, if you're interested in doing it that way? --nick






