Agenda April 14th - Hackweek retrospective

For this retrospective let's focus on the first online hackweek we run, from organizing it to the actual week where we dropped everything and did hack on projects.

1. Add as many items to each list as you think you need to add. - 15 min
2. Vote on 3 items that you think it would be good to discuss. - 5 min


************************************************************************************
******************************* +1 What worked well? *******************************
************************************************************************************
- having space to host informal discussions while working a project worked pretty well e.g. BBB or irc channel+1+1+1
- First time working with other people that aren't part of my daily work routine.
- demo day at the end +1 +1
- presentations on monday [Monday?]
- pad for each group
- being able to join similar groups
- one week to experiment and break routine

- lots of code got merged or is moving towards production.

- having a week without meetings really helped us be productive +1+1+1 +1

- Have one main project with mentoring some subprojects +1

- Mentoring other projects was way more useful than the actual work I did. :)

************************************************************************************
**************** -1 What didn’t work well? What were the challenges? ***************
************************************************************************************

- the lobby space was not used. maybe just irc is fine for the week+1

- lots of one-person projects.  Not so much collaboration on those.
- Fair amount of "yak-shaving" happened.
- expectation of working tools wasn't realistic for many projects.
- There was not always a good entry-point for people who had only a couple of hours here and there to help out with.

- Doing it right before the Easter weekend meant that a lot of people couldn't do Friday, and we missed two weeks of Monday meetings in a row.

- Having "demo" as an ending point didn't always work out for not-so-demoable code.

- projects that were not worked on

- people on short-staffed teams had a hard time leaving behind sponsor work or other fires to work on hackweek projects +1

- There was no follow-up afterwards for people who missed the hackweek and/or missed the presentations at the end: what got hacked on? Is there anything that other people should engage with now?++

- challenge - hard to "pause" work that heavily involves outside collaborators. (Probably nothing to help it, though)++

- didn't really know how to contribute w/ the skills i have as a non engineer/coder+++++

- Tor Community and volunteers engagement. We almost didn't have non-TPI folks collaborating during the hackweek.+1

- Although we postponed some meetings, that doesn't mean we didn't have fires and other business to solve during the week.+1

- it felt a little chaotic in the beginning because some people had other things that week

- Using this pad means that my group didn't have anything to actually say in chat during the split-into-groups retrospective.

************************************************************************************
*********************** What could we try to do differently?  ************************************************************************************

- making sure that communication channels are active and that are more information on how to collaborate

- Pick projects and teams _before_ the week begins, and strongly encourage collaborative work? +1

- Discourage work without a realistic one-week deliverable? (if we care)-1

- Or maybe ways to follow up on the unfinished projects +

- Discuss follow-up plans for *all* projects.  (Several have required post-hackweek work to improve on them, and that's a good thing) +1

- Some more report-outs that live longer than the moment. If you didn't go to the particular hack sessions, and you didn't see the talks at the end, you're completely in the dark about what happened.+++

- Suggestion: explicit note-taking at wrap-up session by somebody other than presenters.

Pick some more projects for non-coders, for example, "we're going to make a brochure for this particular audience" -- then there's something concrete at the end, and maybe it's a thing you wish we had but never found time during busy meeting weeks to focus on.++++1++


- allow space for documenting and sharing spontaneous and smaller tasks that might take place during the week that aren't necessary in the wiki page

    - Make a list of all the external community people we wish we would engage more, and explicitly go out and invite them, have projects in mind that we think would fit them well, and try to get them involved. A hackweek is perfect for growing the community because (unlike dev meetings) nobody has to pay for flights to get there.+1++1

    - Figure out what to do with contributors/teams who can only spend one day, or a few hours here and there. +1
    - I invited some contributors, but they didn't find a project to work on. I think we should have hackweek projects that involve more different skills. Or that you don't need to have deep knowledge of little-t-tor internals. The bar to contribute was a little bit too high.

- have a structure for daily updates from groups maybe a set daily utc time in the lobby where ppl can hang and hear from others +1+1

- Be explicit about which is the goal: increased participation? experimental exploration of possible future work? increased learning by current participants? working code at end-of-week? (I vote for learning and exploration.)

- Turn down projects that don't meet the requirements for the hack week?




Ideas for next hackweek:
       - Cartoons!
       - Translations!
       - Documentation!
       - Media!
       - Outreach!
       - (what else?)

    - announce it in social media

      - have registration form to join each project

      - have an option for the project if accepts volunteers or not

    - better web page that a gitlab page

    - committee for hackweek organizing, with a goal of choosing a balanced set of projects

    - next one in October or September

    - have a place where people can write down projects they would like somebody else to work on
