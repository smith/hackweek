# Tor Hackweek Project: Circuit Padding Simulator

Summary: The Tor circuit padding framework is under active use by researchers to improve padding defenses against website traffic fingerprinting, and onion service circuit fingerprinting. The simulator is in need of update to the latest Tor release, as well as in need of performance improvements, and a more accurate way to deal with time-based 

Skills Needed: Tor unit test framework, Tor C, maybe python

# Team:

* Mike (UTC-5)


