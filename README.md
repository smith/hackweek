# Tor's Hackweek

This repository is being used to document and coordinate current hackweek at Tor. Old hackweek's documentation is archived in 'Archived' folder in the same repository.

<!-- markdown-toc start - Don't edit this section. Run M-x <!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

**Table of Contents**

- [Tor's Hackweek](#tors-hackweek)
	- [Next Online Hackweek: June 27th to July 1st - 2022](#next-online-hackweek-june-27th-to-july-1st-2022)
- [LOUNGE space](#lounge-space)
	- [Monday Opening Session: TBD](#monday-opening-session-tbd)
	- [Friday DEMO Day: 16UTC on the Lounge room](#friday-demo-day-16utc-on-the-lounge-room)
- [PROJECTS/GROUPS](#projectsgroups)
- [Previous Hackweeks](#previous-hackweeks)

<!-- markdown-toc end -->

## Next Online Hackweek: June 27th to July 1st - 2022

 * **When**: Monday June 27th to Friday July 1st - starts each day at TBD.
 * **Where**: Main channel will be #tor in irc.oftc.net . The presentation of projects and the demo of them will happen in a TBD Big Blue Button room.

This hackweek aims to promote small projects that can be round up in 5 days. We will present the list of proposed projects on the first day of the hackweek and other people will be able to join.

Still we encourage you all to look for collaborators to join your team before the presentation of your project on Monday. This hackweek is for anybody interested in Tor and not only for core contributors. You can go look for people to work with you in other spaces other than Tor spaces (it could be social media, mailing lists, discord, forums, etc).

During the hackweek each group will meet whenever they prefer and work on its specific project. At the end of the week (on Friday) we will have a DEMO day where each group will present their work. Each project will have a pad with information about it, how to collaborate, where to meet with the rest of the team, etc.

Each project will list this information:
 - Project/team name
 - Main point of contact for the project/team (timezone you are in)
 - Summary of the work you want to do
 - Pad for the team (the pad will have all the information about the project, where the team meets, planning if necesary, links, etc)
 - Skills needed for the project (for example, someone who knows onionperf, designer, translator, code in rust, writer, etc)
 - Write down how people will be able to collaborate: any specific work that can be done by contributors with less time than a whole week? Does it accept people that can only put a few hours?

# LOUNGE space

We will have the welcome/opening session on a BBB lounge room on Monday as well as the demo day on Friday.

There is going to be a lounge space in irc for the rest of the week so teams can give an update on their work every day at TBD UTC.

ROOM: TBD

# Timeline

- May 27th: Open Call for Projects in Tor Internal and Tor ecosystem
- June 3rd: Open Call for Projects and contributors in the rest of the world (tor-project and social media)
- Monday June 27th: Hackweek begins: a session that team liasons present the project.
- Friday July 1st: Hackweek ends: team demo their work. 


## Monday Opening Session: 16UTC

ROOM: TBD

1. Logistics.
   * main link with information
   * information about lounge every day
   * information about demo day
2. Each group present their project.
3. Each group will go to their meeting place (BBB or irc or whatever else they may choose).
4. There will be a "lounge" space  in irc.oftc.net for people to drop in during the week in case there are questions or comments, etc.

## Friday DEMO Day: 16UTC

ROOM: TBD

1. Each group demo their work.

# PROJECTS/GROUPS

To add yourself to a group use the group's pad please or talk with the team's liasion.

# How to add a new project

People can add new projects via https://hackweek.onionize.space/

# How to add yourself to a team

Each project will have a pad with all the information. On Monday they will be able to add themselves to a project. Organizers for each proposal can start putting together their teams before Monday.

# Questions?

Send a mail to gaba at torproject dot org.

# Previous Hackweeks

- [March 29th to April 2nd - 2021](Archived/2021March29/)
